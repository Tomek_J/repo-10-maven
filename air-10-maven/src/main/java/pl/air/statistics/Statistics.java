package pl.air.statistics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Statistics {

	public static void main(String[] args) throws IOException, EncryptedDocumentException, InvalidFormatException{
		double [] values = {23.23, 25.34, 54332.432, 3242.422, 1, 2, 3, -3423.32};
		
		Statistics s = new Statistics();
		
		List<Double> res = new ArrayList<>(); 
		File txt = new File("C:/AIR/obserwacje.txt");
		File excel = new File("C:/AIR/obserwacje.xlsx");
		
		res = s.readFormattedDouble(txt);
		//res = s.getDataExcel(excel);
		s.printStatistics(res);
	}

	public void printStatistics(double[] inputArray){
		// Get a DescriptiveStatistics instance
		DescriptiveStatistics stats = new DescriptiveStatistics();

		// Add the data from the array
		for( int i = 0; i < inputArray.length; i++) {
		        stats.addValue(inputArray[i]);
		}

		// Compute some statistics
		double mean = stats.getMean();
		double std = stats.getStandardDeviation();
		double median = stats.getPercentile(50);
		
		System.out.println("mean: " + mean);
		System.out.println("std : " + std);
		System.out.println("median: " + median);
	}
	
	public void readDouble() throws IOException
	{
		
		InputStream in  = new FileInputStream("C:/AIR/obserwacje.txt");
 		DataInputStream dataIn = new DataInputStream(in);

 		System.out.println("Warto�ci odczytane z pliku: ");
 		while(dataIn.available() > 0)
 		{
 			double data = dataIn.readDouble();
 			System.out.println(data);
 		}
 		dataIn.close();
	}
	
 	public List<Double> readFormattedDouble(File txt) throws IOException
 	{
 		List<Double> valuesFromFile = new ArrayList<>();
 		
		Reader in = new FileReader(txt);
 		BufferedReader br = new BufferedReader(in); //mozna odczytac tekst jako cala linie i odczyt jest do stringa

 		String line = br.readLine(); //odczyt pierwszego znaku
 		
 		System.out.println("Warto�ci odczytane z pliku: ");
 		while(line != null) //odczyt tak dlugo poki cos jest w pliku
 		{

 			System.out.println(line);
 			line = br.readLine();
 			double val = Double.parseDouble(line);
 			valuesFromFile.add(val);
 		}
 		br.close();
 		
 		//lista -> tablica
 		/*double[] res = new double[valuesFromFile.size()];
 		int i = 0;
 		for(double val : valuesFromFile)
 		{
 			res[i] = val;
 			i++;
 		}*/
 		
 		return valuesFromFile;
 	}
 	
 	public List<Double> getDataExcel(File excel) throws EncryptedDocumentException, InvalidFormatException, IOException{
 		
 		List<Double> valuesFromExcel = new ArrayList<>();
 		
 		Workbook wb = WorkbookFactory.create(excel);
 		
 	    for (Sheet sheet : wb ) {
 	        for (Row row : sheet) {
 	            for (Cell cell : row) {
 	                double v = cell.getNumericCellValue();
 	                valuesFromExcel.add(v);
 	            }
 	        }
 	    }
 	    return valuesFromExcel;
 	}
 	
	public void printStatistics(List<Double> inputArray){
		// Get a DescriptiveStatistics instance
		DescriptiveStatistics stats = new DescriptiveStatistics();

		// Add the data from the array
		for(double value : inputArray) {
		        stats.addValue(value);
		}

		// Compute some statistics
		double mean = stats.getMean();
		double std = stats.getStandardDeviation();
		double median = stats.getPercentile(50);
		
		System.out.println("mean: " + mean);
		System.out.println("std : " + std);
		System.out.println("median: " + median);
	}
}
