package pl.air.qrcode;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QRCodeGenerator {
	
	public static void main(String[] args) throws WriterException, IOException {
		QRCodeGenerator g = new QRCodeGenerator();
		g.generate("trolololololo", 100, 100);
	}

	public void generate(String content, int width, int height) throws WriterException, IOException{
		
		QRCodeWriter writer = 	new QRCodeWriter();
		BitMatrix result = writer.encode(content, BarcodeFormat.QR_CODE, width, height);
		//MatrixToImageWriter.writeToFile(result, "png", new File("C:/AIR/qrcode.png"));
		MatrixToImageWriter.writeToPath(result, "png", Paths.get("C:/AIR/qrcode.png"));
	}
}
